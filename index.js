const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const api = require('./api');

const port = process.env.PORT || 3001;
const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(api());

app.listen(port, () => console.log("Listening on port " + port));
