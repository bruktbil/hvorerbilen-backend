const path = require('path');
const express = require('express');

module.exports = function () {
    const router = express.Router();

    router.get('/carinfo', (req, res) =>
        res.sendFile(path.join(__dirname, 'mockData', 'carInfo.json')));

    router.get('/usedcarinfo', (req, res) =>
        res.sendFile(path.join(__dirname, 'mockData', 'usedCarInfo.json')));

    const orderedWork = path.join(__dirname, 'mockData', 'orderedWork.json');

    router.get('/orderedwork', (req, res) =>
        res.sendFile(orderedWork));

    const comments = path.join(__dirname, 'mockData', 'comments.json');

    router.get('/comments', (req, res) =>
        res.sendFile(comments));

    const logEntries = path.join(__dirname, 'mockData', 'logEntries.json');

    router.get('/logentries', (req, res) =>
        res.sendFile(logEntries));

    return router;
};