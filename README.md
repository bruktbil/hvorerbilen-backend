# Hvor Er Bilen - backend
This is the server for the 'hvorerbilen' project, intended as an ad-hoc, CORS-enabled backend to serve data for the front-end application from port `:3001`.  
## Intended purpose
In its current state, it simply serves static JSON files as per specified in the **/api/index.js** file, where the various routes are made available and mapped to their respective responses.  
Regardless of simplicity, this is an important step in the separation of concerns between the front-end and back-end application; the frond-end application should only be concerned with the visual presentation of the received data, just as the backend should ever be concerned to serve it.   
## Prerequisites
The project is developed using Node.js v6.6.0, but should work well with all versions ranging from 4.0 and up.  
The latest Node.js version (bundled with npm) can be downloaded from the offical [Node.js][node] homepage.

## Getting started
Getting this server up and running is a simple, three-step process.
- clone this repository
- open a terminal and navigate into the cloned folder
- run `npm i` to download the dependencies for the project
  - this is only required prior to the very first launch of the application
- run `npm start` to start the application

[node]: http://nodejs.org